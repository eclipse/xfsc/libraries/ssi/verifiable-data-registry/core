module testplugin

go 1.21

require gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/verifiable-data-registry/core v0.0.0-20231117091837-2a8d90276e51

replace gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/verifiable-data-registry/core => ../..
